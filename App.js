import * as Font from 'expo-font';
import * as Localization from 'expo-localization';
import * as React from 'react';
import i18n from 'i18n-js';
import {
  ActivityIndicator,
  Platform,
  StatusBar,
  StyleSheet,
  UIManager,
  View,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import { SplashScreen } from 'expo';
import { createStackNavigator } from '@react-navigation/stack';
import Colors from '@constants/Colors';
import Layout from '@constants/Layout';
import Routes from '@constants/Routes';
import translations from '@locale';
import BottomTabNavigator from '@navigation/BottomTabNavigator';
import useLinking from '@navigation/useLinking';
import { useDeviceAuthenticating, useUserAuthenticating } from '@hooks/';
import { AuthContext, AuthStateContext, UserStateContext } from '@hooks/auth';
import AuthenticationScreen from '@screens/AuthenticationScreen'


// Init global configuration/values
i18n.translations = translations
const RootStack = createStackNavigator();

// Enables experimental animation animation
if (
  Platform.OS === "android" &&
  UIManager.setLayoutAnimationEnabledExperimental
) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}


export default function App(props) {
  const [isLoadingComplete, setLoadingComplete] = React.useState(false);
  const [initialNavigationState, setInitialNavigationState] = React.useState();
  const containerRef = React.useRef();
  const { getInitialState } = useLinking(containerRef);
  useDeviceAuthenticating()
  const {
    authContext,
    authenticationState,
    userState,
  } = useUserAuthenticating()

  // Load any resources or data that we need prior to rendering the app
  React.useEffect(() => {
    async function loadResourcesAndDataAsync() {
      try {
        SplashScreen.preventAutoHide();

        // Load locale
        i18n.locale = Localization.locale;
        i18n.fallbacks = true

        // Load our initial navigation state
        setInitialNavigationState(await getInitialState());

        // Load fonts
        await Font.loadAsync({
          ...Ionicons.font,
          'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
        });
      } catch (err) {
        // We might want to provide this error information to an error reporting
        // service
        /* eslint-disable-next-line no-console */
        console.warn(err);
      } finally {
        setLoadingComplete(true);
        SplashScreen.hide();
      }
    }
    loadResourcesAndDataAsync();
  }, []);

  const { user } = userState

  return (
    <AuthContext.Provider value={authContext}>
      {!isLoadingComplete && !props.skipLoadingScreen ? (
        <View style={styles.activityIndicator}>
          <ActivityIndicator
            animating={!isLoadingComplete}
            color="#fafafa"
            size="large"
          />
        </View>
      ) : (
        <AuthStateContext.Provider value={authenticationState}>
          <UserStateContext.Provider value={user}>
            <View style={styles.container}>
              {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
              <NavigationContainer
                  initialState={initialNavigationState}
                  ref={containerRef}
              >
                <RootStack.Navigator headerMode={'none'}
                  initialRouteName={Routes.FeedScreen}
                >
                  <RootStack.Screen
                    component={BottomTabNavigator}
                    name={Routes.FeedScreen}
                  />
                  <RootStack.Screen
                    component={AuthenticationScreen}
                    name={Routes.AuthenticationScreen}
                  />
                </RootStack.Navigator>
              </NavigationContainer>
            </View>
          </UserStateContext.Provider>
        </AuthStateContext.Provider>
      )}
    </AuthContext.Provider>
  );
}

const styles = StyleSheet.create({
  activityIndicator: {
    alignItems: 'center',
    backgroundColor: '#000000BB',
    flex: 1,
    height: Layout.window.height,
    justifyContent: 'center',
    position: 'absolute',
    width: Layout.window.width,
    zIndex: 10,
  },
  container: {
    backgroundColor: Colors.backgroundColor,
    flex: 1,
  },
});
