import React from 'react'
import PropTypes from 'prop-types'
import { API_URL } from '@config/'

useEventFetching.propTypes = {
  urlParams: PropTypes.shape({
    perPage: PropTypes.string,
    sort: PropTypes.string,
    tags: PropTypes.string,
  })
}

useEventFetching.defaultProps = {
  urlParams: {
    perPage: '4',
    sort: '-created_at',
    tags: '',
  }
}

export function useEventFetching({
  urlParams: {
    perPage = '4',
    sort = '-created_at',
    tags = '',
  } = {}
} = {}) {  
  const [isFetchingComplete, setFetchingComplete] = React.useState(false);
  const [eventsState, setEventsState] = React.useState({
    results: [],
    total: 0,
  })

  const hostUrl = API_URL
  const perPageParam = `&per_page=${perPage}`
  const sortParam = `&sort=${sort}`
  const byTagsParam = tags && `&tags=${tags}`
  const params = `${perPageParam}${sortParam}${perPageParam}${byTagsParam}`
  const first_page_url = `${hostUrl}/api/events/?${params}`
  const [apiState, setApiState] = React.useState({
    next_page_url: first_page_url
  })

  function getFilteredEvents(events) {
    const { results: currentList } = eventsState
    const eventIds = events.map(e => e.id)
    const currentListIds = currentList.map(e => e.id)
    return eventIds.reduce(
      (accumulator, nextValue, index) => {
        if (!currentListIds.includes(nextValue)) {
          accumulator.push(events[index])
        }
        return accumulator
      }, []
    )
  }

  async function fetchEvents(url) {
    const response = await fetch(url);
    return await response.json();
  }

  async function onRefreshFeed() {
    try {
      // Fetches events
      const eventsResponse = await fetchEvents(first_page_url)
      // Filters new events against those previously fetched
      const { results: responseResults, total } = eventsResponse
      const filteredEvents = getFilteredEvents(responseResults)
      // Updates the elements to the feed state
      setEventsState({
        ...eventsState,
        results: filteredEvents.concat(eventsState.results),
        total,
      })
    } catch(err) {
      /* eslint-disable-next-line no-console */
      console.warn(err)
    } finally {
      setFetchingComplete(true)
    }
  }

  async function getFeed() {
    try {
      const { next_page_url } = apiState
      if (next_page_url === null) {
        return;
      }
      const { results: currentEvents } = eventsState
      // Fetches events
      const eventsResponse = await fetchEvents(apiState.next_page_url)
      // Filters new events against those previously fetched
      const {
        results: responseResults,
        next_page_url: updated_next_page_url,
        total,
      } = eventsResponse
      const filteredEvents = getFilteredEvents(responseResults)
      // Adds elements to the feed state
      const updatedList = currentEvents.concat(filteredEvents)
      setEventsState({ total, results: updatedList })
      // Updates api state with the next page url
      setApiState({ next_page_url: updated_next_page_url })
    } catch (err) {
      /* eslint-disable-next-line no-console */
      console.warn(err)
    } finally {
      setFetchingComplete(true)
    }
  }

  React.useEffect(() => {
    // FIXME: use cancelable definition or similar
    // https://reactjs.org/blog/2015/12/16/ismounted-antipattern.html
    let isSubscribed = true
    isSubscribed && getFeed()
    return () => isSubscribed = false
  }, [])

  return {
    data: eventsState.results,
    getFeed: getFeed,
    hostUrl: hostUrl,
    onRefresh: onRefreshFeed,
    refreshing: !isFetchingComplete,
  }
}
