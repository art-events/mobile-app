import React from 'react'
import { API_URL, DEVICE_ID } from '@config/'
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import { AsyncStorage } from 'react-native'
import i18n from 'i18n-js';

export const AuthContext = React.createContext()
export const AuthStateContext = React.createContext()
export const UserStateContext = React.createContext()

export function useUserAuthenticating() {
  async function signIn({ username, password }) {
    const url = `${API_URL}/api-token-auth/`
    return fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ email: username, password })
    })
  }

  async function getCurrentUser(token) {
    const url = `${API_URL}/api/users/current`
    return fetch(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `JWT ${token}`
      },
    })
  }

  const authContext = React.useMemo(() => ({
    signIn: async data => {
      try {
        dispatch({ type: 'SIGN_IN_REQUEST' })
        const signInResponse = await signIn(data)
        if (signInResponse.status !== 200) {
          throw new Error('Authentication with username and password failed')
        }
        const parsedSignInResponse = await signInResponse.json()
        const { token } = parsedSignInResponse
        AsyncStorage.setItem('userToken', token)

        dispatchUserState({ type: 'GET_CURRENT_USER_REQUEST' })
        const currentUserReponse = await getCurrentUser(token)
        if (currentUserReponse.status !== 200) {
          throw new Error('Current user request failed')
        }
        const parsedCurrentUserResponse = await currentUserReponse.json()
        dispatchUserState({
          type: 'GET_CURRENT_USER_SUCCESS',
          user: parsedCurrentUserResponse,
        })
        dispatch({
          type: 'SIGN_IN_SUCCESS',
          token: parsedSignInResponse.token,
        })
      } catch (err) {
        /* eslint-disable no-console */
        console.warn(err.message)
        dispatch({ type: 'SIGN_IN_FAILURE' });
        dispatchUserState({ type: 'GET_CURRENT_USER_FAILURE' })
      }
    },
    signOut: async () => {
      console.log('Signed Out!')
      await AsyncStorage.removeItem('userToken')
      dispatch({ type: 'SIGN_OUT' })
      dispatchUserState({ type: 'GET_CURRENT_USER_FAILURE '})
    },
    signUp: async data => {
      // In a production app, we need to send user data to server and get token
      // We will also need to handle errors if sign up failed
      // After getting token, we need to persist the token using `AsyncStorage`
      // In the example, we'll use a dummy token
      console.log('Sign Up data ::: ', data)
      // dispatch({ type: 'SIGN_IN', token: 'dummy-auth-token' });
    },
  }), []);

  const [userState, dispatchUserState] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'GET_CURRENT_USER_REQUEST':
          return {
            ...prevState,
            isLoading: true,
          }
        case 'GET_CURRENT_USER_SUCCESS':
          return {
            ...prevState,
            isLoading: false,
            user: {
              createdAt: action.user.created_at,
              email: action.user.email,
              firstName: action.user.first_name,
              groups: action.user.groups,
              id: `${action.user.id}`,
              lastName: action.user.last_name,
              updatedAt: action.user.updated_at,
              username: action.user.username,
            }
          }
        case 'GET_CURRENT_USER_FAILURE':
          return {
            ...prevState,
            isLoading: false,
            user: {
              createdAt: '',
              email: '',
              firstName: '',
              groups: [],
              id: '',
              lastName: '',
              updatedAt: '',
              username: '',
            }
          }
      }
    }, {
      isLoading: false,
      user: {
        createdAt: '',
        email: '',
        firstName: '',
        groups: [],
        id: '',
        lastName: '',
        updatedAt: '',
        username: '',
      }
    }
  )

  const [authenticationState, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'VERIFY_TOKEN_REQUEST':
          return {
            ...prevState,
            isLoading: true,
          };
        case 'VERIFY_TOKEN_SUCCESS':
          return {
            ...prevState,
            isFirstTime: action.isFirstTime,
            isLoading: false,
            userToken: action.token,
          };
        case 'VERIFY_TOKEN_FAILURE':
          return {
            ...prevState,
            isLoading: false,
            userToken: null,
            isFirstTime: false,
          };
        case 'SIGN_IN_REQUEST':
          return {
            ...prevState,
            hasErrors: false,
            isLoading: true,
          };
        case 'SIGN_IN_SUCCESS':
          return {
            ...prevState,
            hasErrors: false,
            isLoading: false,
            isSignout: false,
            userToken: action.token,
          };
        case 'SIGN_IN_FAILURE':
          return {
            ...prevState,
            hasErrors: true,
            isSignout: false,
            userToken: null,
            isLoading: false
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            hasErrors: false,
            isSignout: true,
            userToken: null,
          };
      }
    },
    {
      hasErrors: false,
      isFirstTime: true,
      isLoading: false,
      isSignout: false,
      userToken: null,
    }
  );

  React.useEffect(() => {
    async function verifyToken(token) {
      const url = `${API_URL}/api-token-verify/`
      return fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ token })
      })
    }

    async function authenticationBootstrapAsync() {
      let userToken;
      // await AsyncStorage.removeItem('userToken')
      try {
        dispatch({ type: 'VERIFY_TOKEN_REQUEST' })
        userToken = await AsyncStorage.getItem('userToken');
        if (userToken !== null) {
          const response = await verifyToken(userToken)
          const { token } = await response.json()
          if (response.status !== 200) {
            throw new Error('Token verification failed.')
          }
          dispatchUserState({ type: 'GET_CURRENT_USER_REQUEST' })
          const currentUserReponse = await getCurrentUser(token)
          if (currentUserReponse.status !== 200) {
            throw new Error('Current user request failed')
          }
          const parsedCurrentUserResponse = await currentUserReponse.json()
          dispatchUserState({
            type: 'GET_CURRENT_USER_SUCCESS',
            user: parsedCurrentUserResponse,
          })
          dispatch({
            type: 'VERIFY_TOKEN_SUCCESS',
            token,
            isFirstTime: false
          });
        } else {
          throw new Error('No token found in storage.')
        }
        // No user token was found, state remains the same: it's a new user.
      } catch (err) {
        /* eslint-disable-next-line no-console */
        console.warn(err)
        dispatch({ type: 'VERIFY_TOKEN_FAILURE', token: null })
      }
    }

    authenticationBootstrapAsync()
  }, [])

  return {
    authContext,
    authenticationState,
    userState
  }
}

export function useDeviceAuthenticating() {
  const [authState, setAuthState] = React.useState({})

  const hostUrl = API_URL
  const serviceUrl = 'accounts/device'
  const url = `${hostUrl}/api/${serviceUrl}`

  async function registerForPushNotificationsAsync() {
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    // only asks if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    // On Android, permissions are granted on app installation, so
    // `askAsync` will never prompt the user

    // Stop here if the user did not grant permissions
    if (status !== 'granted') {
      alert(i18n.t('no-notifications-permission'));
      return;
    }

    // Get the token that identifies this device
    return await Notifications.getExpoPushTokenAsync();
  }

  function fetchDevice(url, token) {
    return fetch(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Device-Id': DEVICE_ID,
        'Expo-Push-Token': token
      },
    })
  }

  React.useEffect(() => {
    async function authenticate() {
      try {
        const token = await registerForPushNotificationsAsync()
        const response = await fetchDevice(url, token)
        const parsedResponse = await response.json()
        setAuthState(parsedResponse)
      } catch (err) {
        /* eslint-disable-next-line no-console */
        console.warn(err)
      }
    }

    let isSubscribed = true
    isSubscribed && authenticate()
    return () => isSubscribed = false
  }, [])

  return {
    user: authState
  }
}
