import React from 'react'
import { API_URL } from '@config/'

export const useTagsFetching = () => {
  const [isFetchingComplete, setFetchingComplete] = React.useState(false);
  const [tagsState, setTagsState] = React.useState([])
  
  async function fetchTags() {
    try {
      const tagsServiceUrl = 'api/tags'
      const url = `${API_URL}/${tagsServiceUrl}`
      const response = await fetch(url)
      const parsedResponse = await response.json()
      setTagsState(parsedResponse.results)
    } catch (err) {
      /* eslint-disable-next-line no-console */
      console.warn(err)
    } finally {
      setFetchingComplete(true)
    }
  }
  
  React.useEffect(() => {
    fetchTags()
  }, [])
  
  return {
    data: tagsState,
    refreshing: !isFetchingComplete,
  }
}