export default {
  /* Header titles */
  'bottomtab-home-label': 'Inicio',
  'bottomtab-home-aria-label': 'Sección de inicio',
  'bottomtab-search-label': 'Buscar',
  'bottomtab-search-aria-label': 'Sección de búsqueda',
  'bottomtab-settings-label': 'Configuración',
  'bottomtab-settings-aria-label': 'Sección de configuración',

  /* Authentication Stack Screens */

  // Fields
  'auth-form-email-placeholder': 'correo electrónico',
  'auth-form-repeat-password-placeholder': 'repetí la contraseña',
  'auth-form-password-placeholder': 'contraseña',
  'auth-form-username-placeholder': 'nombre de usuarix',
  'auth-form-go-back-button': 'Volver...',
  // Messages
  'auth-screen-message': 'Querés ingresar o registrarte?',
  'sign-in-screen-message': 'Completa el formulario por favor',
  'sign-up-screen-message': 'Completa el formulario por favor',
  // Buttons
  'auth-screen-sign-in-button': 'Ingresar',
  'auth-screen-sign-up-button': 'Registrarse',
  'sign-in-screen-submit-button': 'Ingresar!',
  'sign-up-screen-submit-button': 'Registrame!',

  /* Home Screen */
  'visitor-login-register-button':
    'Haz click aquí para ingresar o registrarte!',

  /* Search Stack Screen */
  'search-details-multiple-route-name': 'Selección múltiple',
  'search-tags-button-label': '¡Buscar eventos!',

  // Accessibility labels
  'search-by-tags-aria-label': 'Búsqueda por múltiples opciones',

  // Alerts
  'no-notifications-permission': 'Sin permisos para recibir notificaciones',

  /* Screen headers and Bottom Tag Navigation */
  welcome: 'Bienvenidx',
  welcomeUser: 'Bienvenidx %{username}',
  search: 'Búsqueda',
  searchDetails: 'Detalles de búsqueda',
  settings: 'Configuración',
  settingsDetails: 'Detalles de configuración',
}
