export default {
  /* Header titles */
  'bottomtab-home-label': 'Home',
  'bottomtab-home-aria-label': 'Home section',
  'bottomtab-search-label': 'Search',
  'bottomtab-search-aria-label': 'Search section',
  'bottomtab-settings-label': 'Settings',
  'bottomtab-settings-aria-label': 'Configuration section',

  /* Authentication Stack Screens */

  // Fields
  'auth-form-email-placeholder': 'email address',
  'auth-form-repeat-password-placeholder': 'repeat password',
  'auth-form-password-placeholder': 'password',
  'auth-form-username-placeholder': 'username',
  'auth-form-go-back-button': 'Go back...',
  // Messages
  'auth-screen-message': 'Would you like to Sign in or Register?',
  'sign-in-screen-message': 'Please complete the fields below',
  'sign-up-screen-message': 'Please complete the fields below',
  // Buttons
  'auth-screen-sign-in-button': 'Sign in',
  'auth-screen-sign-up-button': 'Sign up',
  'sign-in-screen-submit-button': 'Let me in!',
  'sign-up-screen-submit-button': 'Sign me up!',

  /* Home Screen */
  'visitor-login-register-button':
    'Hello visitor, click here to register or log in!',

  /* Search Stack Screen */
  'search-details-multiple-route-name': 'Multiple categories',
  'search-tags-button-label': '¡Buscar eventos!',

  // Accessibility labels
  'search-by-tags-aria-label': 'Search by multiple categories button',

  // Alerts
  'no-notifications-permission': 'No permissions to receive notifications',

  /* Screen headers and Bottom Tag Navigation */
  welcome: 'Hello',
  welcomeUser: 'Hello %{username}',
  search: 'Search',
  searchDetails: 'Search details',
  settings: 'Configuration',
  settingsDetails: 'Configuration details',
}
