import * as React from 'react';
import PropTypes from 'prop-types'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import i18n from 'i18n-js';
import TabBar from '@components/TabBar'
import TabBarIcon from '@components/TabBarIcon';
import Colors from '@constants/Colors'
import Routes from '@constants/Routes'
import HomeScreen from '@screens/HomeScreen';
import SearchStackScreen from '@screens/SearchStackScreen'
import SettingsScreen from '@screens/SettingsScreen';

const BottomTab = createBottomTabNavigator();


BottomTabNavigator.propTypes = {
  navigation: PropTypes.object,
  route: PropTypes.object
}

export default function BottomTabNavigator() {
  return (
    <BottomTab.Navigator
      initialRouteName={Routes.FeedScreen}
      tabBar={props => <TabBar {...props} />}
      tabBarOptions={{
        activeTintColor: Colors.tabIconSelected,
        inactiveTintColor: Colors.tabIconDefault,
        showLabel: false
      }}
    >
      <BottomTab.Screen
        component={HomeScreen}
        name={Routes.FeedScreen}
        options={{
          tabBarAccessibilityLabel: i18n.t('bottomtab-home-aria-label'),
          tabBarLabel: i18n.t('bottomtab-home-label'),
          tabBarIcon: ({ color, focused }) => {
            return (
              <TabBarIcon color={color} focused={focused} name="md-home" />
            )
          },
        }}
      />
      <BottomTab.Screen
        component={SearchStackScreen}
        name={Routes.SearchScreen}
        options={{
          tabBarAccessibilityLabel: i18n.t('bottomtab-search-aria-label'),
          tabBarLabel: i18n.t('bottomtab-search-label'),
          tabBarIcon: ({ color, focused }) => (
            <TabBarIcon color={color} focused={focused} name="md-search" />
          )
        }}
      />
      <BottomTab.Screen
        component={SettingsScreen}
        name={Routes.SettingsScreen}
        options={{
          tabBarAccessibilityLabel: i18n.t('bottomtab-settings-aria-label'),
          tabBarLabel: i18n.t('bottomtab-settings-label'),
          tabBarIcon: ({ color, focused }) => (
            <TabBarIcon color={color} focused={focused} name="md-cog" />
          ),
        }}
      />
    </BottomTab.Navigator>
  );
}
