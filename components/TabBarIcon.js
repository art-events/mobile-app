import * as React from 'react';
import PropTypes from 'prop-types'
import { Ionicons } from '@expo/vector-icons';

TabBarIcon.propTypes = {
  color: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  size: PropTypes.number,
}

TabBarIcon.defaultProps = {
  size: 30
}

export default function TabBarIcon(props) {
  return (
    <Ionicons
      color={props.color}
      name={props.name}
      size={props.size}
      style={{ marginBottom: -3, textAlign: 'center' }}
    />
  );
}
