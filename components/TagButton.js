import React from 'react'
import PropTypes from 'prop-types'
import {
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import Layout from '@constants/Layout'


TagButton.propTypes = {
  name: PropTypes.string.isRequired,
  onLongPress: PropTypes.func.isRequired,
  onPress: PropTypes.func.isRequired,
}

export default function TagButton({ name, onLongPress, onPress }) {
  const [selectedState, setSelectedState] = React.useState(false)

  function _onLongPress() {
    const _selectedState = onLongPress()
    setSelectedState(_selectedState)
  }

  function _onPress() {
    const _selectedState = onPress()
    setSelectedState(_selectedState)
  }

  return (
    <TouchableOpacity
      activeOpacity={0.5}
      onLongPress={_onLongPress}
      onPress={_onPress}
      style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
    >
      <View
        accessibilityRole={'button'}
        activeOpacity={0}
        style={{
          alignItems: 'center',
          backgroundColor: selectedState ? '#fafafaAA' : '#000000AA',
          borderColor: selectedState ? '#fafafaBB' : '#000000BB',
          borderRadius: 100,
          borderWidth: 1,
          height: Layout.window.width/4,
          justifyContent: 'center',
          margin: 20,
          padding: 5,
          width: Layout.window.width/4,
        }}
      >
        <Text
          accessibilityRole={'button'}
          style={{
            color: selectedState ? '#000' : '#fafafa',
            fontSize: 18,
            textAlign: 'center',
          }}
        >{name}</Text>
      </View>
    </TouchableOpacity>
  )
}
