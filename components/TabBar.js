import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    height: 50,
    justifyContent: 'center',
  }
})

TabBar.propTypes = {
  state: PropTypes.object,
  descriptors: PropTypes.object,
  navigation: PropTypes.object,
  activeTintColor: PropTypes.string,
  inactiveTintColor: PropTypes.string,
  // It's unexpectedly receiving undefined from the Bottom Navigator,should be
  // boolean
  showLabel: PropTypes.any,
}

TabBar.defaultProps = {
  showLabel: false
}

export default function TabBar({ state, descriptors, navigation, ...props }) {
  return (
    <View style={{ flexDirection: 'row' }}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const { tabBarLabel, tabBarIcon: TabBarIcon } = options
        const { activeTintColor, inactiveTintColor, showLabel } = props
        const isFocused = state.index === index;

        const color = isFocused ? activeTintColor : inactiveTintColor 

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            accessibilityLabel={options.tabBarAccessibilityLabel}
            accessibilityRole="button"
            accessibilityStates={isFocused ? ['selected'] : []}
            key={index}
            onLongPress={onLongPress}
            onPress={onPress}
            style={styles.container}
            testID={options.tabBarTestID}
          >
            <TabBarIcon color={color} />
            {
              showLabel && (
                <Text style={{ color }}>{tabBarLabel}</Text>
              )
            }
          </TouchableOpacity>
        );
      })}
    </View>
  );
}
