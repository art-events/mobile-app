import React from 'react';
import PropTypes from 'prop-types'
import { View, StyleSheet, Text, TouchableHighlight } from 'react-native'
import Image from 'react-native-scalable-image';
import { Ionicons } from '@expo/vector-icons';
import Layout from '@constants/Layout'
import { API_URL } from '@config/'

const { window: { width, height } } = Layout

EventCard.propTypes = {
  description: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  pictureUrl: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
}

export default function EventCard({ description, id, pictureUrl, title }) {
  const [truncatedState, setTruncatedState] = React.useState(3)
  const [likeButtonState, setLikeButtonState] = React.useState(false)

  function getLikeIconColor() {
    if (likeButtonState) {
      return 'red'
    } else {
      return 'white'
    }
  }

  let _pictureUrl = pictureUrl
  if (!__DEV__) {
    _pictureUrl = `${API_URL}/static/images/events/${id%7}.jpg`
  }

  return (
    <View style={styles.item}>
      <View>
        <Image
          height={height}
          source={{ uri: _pictureUrl }}
          width={width-8}
        >
        </Image>
      </View>
      <View style={styles.actionsBar}>
        <TouchableHighlight>
          <Ionicons
            color={getLikeIconColor()}
            name={'md-heart'}
            onPress={() => setLikeButtonState(!likeButtonState)}
            size={34}
            style={{ marginBottom: -3 }}
          />
        </TouchableHighlight>
        <TouchableHighlight style={{paddingLeft: 16}}>
          <Ionicons
            color={'white'}
            name={'md-share'}
            size={34}
            style={{ marginBottom: -3 }}
          />
        </TouchableHighlight>
      </View>
      <View style={styles.itemBottom}>
        <Text
          ellipsizeMode={'tail'}
          numberOfLines={truncatedState}
          onPress={() => setTruncatedState(0)}
          style={styles.description}
        >
          <Text style={styles.title}>{title}</Text>
          {` ${description}`}
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  actionsBar: {
    backgroundColor: '#000000DA',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    padding: 8,
  },
  description: {
  },
  item: {
    backgroundColor: '#fafafa',
    borderRadius: 5,
    marginHorizontal: 4,
    marginVertical: 4,
  },
  itemBottom: {
    flex: 4,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    padding: 4,
  },
  // itemCenter: {
  //   backgroundColor: 'steelblue',
  //   borderTopLeftRadius: 5,
  //   borderTopRightRadius: 5,
  //   flex:1,
  //   height: Layout.window.height/3,
  //   padding: 10,
  // },
  title: {
    fontWeight: 'bold'
  },
});
