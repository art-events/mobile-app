import Constants from 'expo-constants';

const {
  deviceId,
  manifest: { extra: _config },
} = Constants
const environment = __DEV__ ? 'develop' : 'staging'
const config = _config[environment]
const { server: { host } } = config

export const API_URL = host
export const DEVICE_ID = deviceId
