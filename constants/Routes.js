
export default {
  INITIAL_ROUTE_NAME: 'HomeScreen',

  /* Root Screen */
  RootScreen: 'RootScreen',
  
  /* Authentication Screen */
  AuthenticationScreen: 'AuthenticationScreen',
  SignInScreen: 'SignInScreen',
  SignUpScreen: 'SignUpScreen',

  /* Home Stack */
  FeedScreen: 'FeedScreen',

  /* Search Stack */
  SearchScreen: 'SearchScreen',
  SearchDetailsScreen: 'SearchDetailsScreen',

  /* Settings Stack */
  SettingsScreen: 'SettingsScreen',
  SettingsDetailsScreen: 'SettingsDetailsScreen',
}
