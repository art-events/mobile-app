import { Dimensions } from 'react-native';
import Constants from 'expo-constants'

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default {
  isSmallDevice: width < 375,
  statusBarHeight: Constants.statusBarHeight,
  window: {
    width,
    height,
  },
};
