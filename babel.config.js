module.exports = function(api) {
  api.cache(true);
  return {
    presets: ['babel-preset-expo'],
    plugins: [
      [
        'module-resolver',
        {
          alias: {
            '@assets': './assets',
            '@components': './components',
            '@config': './config',
            '@constants': './constants',
            '@hooks': './hooks',
            '@locale': './locale',
            '@navigation': './navigation',
            '@screens': './screens'
          }
        }
      ]
    ]
  };
};
