module.exports = {
  plugins: ['react', 'react-native', 'react-hooks'],
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:react-native/all'
  ],
  settings: {
    react: {
      version: '16',
    },
  },
  env: {
    browser: true,
    node: true,
  },
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module',
    ecmaVersion: 6,
    ecmaFeatures: {
      jsx: true,
      modules: true,
      experimentalObjectRestSpread: true,
    },
  },
  rules: {
    'max-len': 'error',
    'no-console': 'error',
    'react/display-name': 'off',
    'react/jsx-sort-props': 'error',
    'react/prop-types': 'warn',
    'react-native/no-inline-styles': 'off',
    'react-native/no-color-literals': 'off',
  },
  globals: {
    '__DEV__': 'readonly',
  }
};
