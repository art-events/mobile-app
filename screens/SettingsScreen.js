import * as React from 'react';
import PropTypes from 'prop-types'
import i18n from 'i18n-js';
import { RectButton, ScrollView } from 'react-native-gesture-handler';
import {
  ImageBackground,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { createStackNavigator } from '@react-navigation/stack'
import Routes from '@constants/Routes'


export function getHeaderTitle(route) {
  const routeName = route.name
  switch (routeName) {
    case Routes.SettingsScreen:
      return i18n.t('settings');
    case Routes.SettingsDetailsScreen:
      return i18n.t('settingsDetails');
  }
}

const SettingsStack = createStackNavigator();

export default () => {
  return (
    <SettingsStack.Navigator
      headerMode={'float'}
    >
      <SettingsStack.Screen
        component={SettingsScreen}
        name={Routes.SettingsScreen}
        options={({ route }) => ({
          headerTitle: getHeaderTitle(route),
        })}
      />
      <SettingsStack.Screen
        component={SettingsDetailsScreen}
        name={Routes.SettingsDetailsScreen}
        options={({ route }) => ({
          headerTitle: getHeaderTitle(route)
        })}
      />
    </SettingsStack.Navigator>
  )
}


SettingsScreen.propTypes = {
  navigation: PropTypes.object
}

function SettingsScreen({ navigation }) {
  return (
    <ImageBackground
      source={require('@assets/images/bg/settings-bg.jpg')}
      style={{
        color: '#000',
        flex: 1,
        resizeMode: "cover",
      }}
    >
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <TouchableHighlight
          onPress={() => navigation.navigate(Routes.SettingsDetailsScreen)}
        >
          <View
            style={{
              backgroundColor: '#fafafa00',
              borderRadius: 100,
              height: 100,
              width: 100,
            }}
          />
        </TouchableHighlight>
      </View>
    </ImageBackground>
  )
}

function SettingsDetailsScreen() {
  return (
    <ScrollView
      contentContainerStyle={styles.contentContainer}
      style={styles.container}
    >
      <OptionButton
        icon="md-school"
        label=" ۜ\(סּںסּَ` )/ۜ"
        onPress={() => {}}
      />
    </ScrollView>
  );
}

function OptionButton({ icon, label, onPress, isLastOption }) {
  return (
    <RectButton
      onPress={onPress}
      style={[styles.option, isLastOption && styles.lastOption]}
    >
      <View style={{ flexDirection: 'row' }}>
        <View style={styles.optionIconContainer}>
          <Ionicons color="rgba(0,0,0,0.55)" name={icon} size={22} />
        </View>
        <View style={styles.optionTextContainer}>
          <Text style={styles.optionText}>{label}</Text>
        </View>
      </View>
    </RectButton>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#000',
    flex: 1,
  },
  contentContainer: {
    paddingTop: 15,
  },
  lastOption: {
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  option: {
    backgroundColor: '#fafafaDD',
    borderBottomWidth: 0,
    borderColor: '#fafafaEE',
    borderWidth: StyleSheet.hairlineWidth,
    paddingHorizontal: 15,
    paddingVertical: 15,
  },
  optionIconContainer: {
    marginRight: 12,
  },
  optionText: {
    alignSelf: 'flex-start',
    color: '#000000AA',
    fontSize: 15,
    marginTop: 1,
  },
});
