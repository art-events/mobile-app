import * as React from 'react';
import PropTypes from 'prop-types'
import i18n from 'i18n-js';
import {
  Button,
  FlatList,
  ImageBackground,
  LayoutAnimation,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { createStackNavigator } from '@react-navigation/stack';
import EventCard from '@components/EventCard'
import TagButton from '@components/TagButton'
import Routes from '@constants/Routes'
import { useEventFetching, useTagsFetching } from '@hooks/'


HeaderTitle.propTypes = {
  icon: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
}

function HeaderTitle({ icon, text }) {
  return (
    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
      <Ionicons
        color="#000000AA"
        name={icon}
        size={26}
        style={{ paddingLeft: 0, marginRight: 5 }}
      />
      <Text style={{ fontSize: 20 }}>{text}</Text>
    </View>  
  )
}

export function getHeaderTitle(route) {
  const routeName = route.name
  let icon = ''
  let text = ''
  switch (routeName) {
    case Routes.SearchScreen:
      return () => (
        <HeaderTitle icon={'md-search'} text={i18n.t('search')} />
      )
    case Routes.SearchDetailsScreen:
      if (route.params.tags.length > 1) {
        icon = 'md-pricetags'
        text = i18n.t('search-details-multiple-route-name')
      } else {
        icon = 'md-pricetag'
        text = route.params?.name
      }
      return () => <HeaderTitle icon={icon} text={text} />
  }
}


const SearchStack = createStackNavigator();

export default () => {
  return (
    <SearchStack.Navigator
      headerMode={'float'}
    >
      <SearchStack.Screen
        component={SearchScreen}
        name={Routes.SearchScreen}
        options={({ route }) => ({
          headerTitle: getHeaderTitle(route)
        })}
      />
      <SearchStack.Screen
        component={SearchDetailsScreen}
        name={Routes.SearchDetailsScreen}
        options={({ route }) => ({
          headerTitle: getHeaderTitle(route)
        })}
      />
    </SearchStack.Navigator>
  )
}

SearchScreen.propTypes = {
  navigation: PropTypes.object
}


function SearchScreen({ navigation }) {
  const [selectedTagsState, setSelectedTagsState] = React.useState([])
  const [isSelectionEnabled, setSelectionState] = React.useState(false)

  function handleLongPress(item) {
    if (!isSelectionEnabled) {
      return () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.spring)
        setSelectionState(true)
        setSelectedTagsState([...selectedTagsState, item.id])
        return true
      }
    }
    return () => {
      return false
    }
  }

  function handleOnPress(item) {
    if (!isSelectionEnabled) {
      return () => {
        navigation.navigate(Routes.SearchDetailsScreen, {
          name: item.name,
          tags: [item.id],
        })
        return false
      }
    }
    return () => {
      let currentTags = [...selectedTagsState]

      const currentItemExists = selectedTagsState.some(e => e === item.id)
      if (currentItemExists) {
        currentTags = currentTags.filter(e => e !== item.id)
      } else {
        currentTags.push(item.id)
      }
      setSelectedTagsState(currentTags)

      const isCurrentTagsListEmpty = currentTags.length === 0
      if (isCurrentTagsListEmpty) {
        setSelectionState(false)
        LayoutAnimation.configureNext(LayoutAnimation.Presets.spring)
      }

      return !currentItemExists
    }
  }

  function onPress() {
    navigation.navigate(Routes.SearchDetailsScreen, {
      name: i18n.t('search-details-multiple-route-name'),
      tags: selectedTagsState,
    })
  }

  const { data, refreshing } = useTagsFetching()

  React.useEffect(() => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring)
  }, [])

  return (
    <ImageBackground
      source={require('@assets/images/bg/search-bg.jpg')}
      style={{
        color: '#000',
        flex: 1,
        resizeMode: "cover",
      }}
    >
      <SafeAreaView style={{
        flex: 1,
      }}>
        <FlatList
          data={data}
          keyExtractor={item => `${item.id}`}
          numColumns={2}
          refreshing={refreshing}
          renderItem={({ item }) => (
            <TagButton
              name={item.name}
              onLongPress={handleLongPress(item)}
              onPress={handleOnPress(item)}
            />
          )}
        />
        {
          isSelectionEnabled &&
          <View>
            <Button
              accessibilityLabel={i18n.t('search-by-tags-aria-label')}
              color="#000"
              disabled={!isSelectionEnabled}
              onPress={onPress}
              title={i18n.t('search-tags-button-label')}
            />
          </View>
        }
      </SafeAreaView>
    </ImageBackground>
  )
}


SearchDetailsScreen.propTypes = {
  route: PropTypes.object
}

function SearchDetailsScreen({ route }) {
  const urlParams = {
    tags: route.params.tags
  }
  const {
    data, getFeed, onRefresh, refreshing, hostUrl
  } = useEventFetching({ urlParams })
  return (
    <View style={styles.container}>
      <SafeAreaView>
        <FlatList
          data={data}
          keyExtractor={item => `${item.id}`}
          onEndReached={getFeed}
          onRefresh={onRefresh}
          refreshing={refreshing}
          renderItem={({ item }) => (
            <EventCard
              description={item.description}
              id={item.id}
              pictureUrl={`${hostUrl}/${item.picture_file}`}
              title={item.title}
            />
          )}
        />
      </SafeAreaView>
    </View>
  )
}


const styles = StyleSheet.create({
  container: {
    backgroundColor: '#000',
    flex: 1,
  },
});