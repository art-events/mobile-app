import * as React from 'react';
import i18n from 'i18n-js';
import {
  Button,
  FlatList,
  SafeAreaView,
  StyleSheet,
  View,
} from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import EventCard from '@components/EventCard'
import Layout from '@constants/Layout'
import Routes from '@constants/Routes'
import { useEventFetching } from '@hooks/'
import { AuthStateContext, UserStateContext } from '@hooks/auth'


export function getHeaderTitle(route) {
  const user = React.useContext(UserStateContext)
  const { userToken } = React.useContext(AuthStateContext)

  const routeName = route.name
  switch (routeName) {
    case Routes.FeedScreen:
      if (userToken !== null) {
        const { username } = user
        return i18n.t('welcomeUser', {username});
      }
      return i18n.t('welcome');
  }
}

const HomeStack = createStackNavigator();

export default () => {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen
        component={FeedScreen}
        name={Routes.FeedScreen}
        options={({ route }) => ({
          headerTitle: getHeaderTitle(route)
        })}
      />
    </HomeStack.Navigator>
  )
}


function FeedScreen({ navigation, route }) {
  const user = React.useContext(UserStateContext)
  const { userToken } = React.useContext(AuthStateContext)
  const {
    data, getFeed, onRefresh, refreshing, hostUrl
  } = useEventFetching()
  
  return (
    <View style={styles.container}>
      <SafeAreaView>
      {userToken === null ? (
        <View
          style={{
            position: 'absolute',
            top: 0,
            width: Layout.window.width,
            // for some reason is not clickeable without zIndex = 1
            zIndex: 1,
          }}>
          <Button
            color={'#000'}
            onPress={() => { navigation.navigate(Routes.AuthenticationScreen)}}
            title={i18n.t('visitor-login-register-button')}
          />
        </View>
      ) : null
      }
        <FlatList
          data={data}
          keyExtractor={item => `${item.id}`}
          onEndReached={getFeed}
          onRefresh={onRefresh}
          refreshing={refreshing}
          renderItem={({ item }) => (
            <EventCard
              description={item.description}
              id={item.id}
              pictureUrl={`${hostUrl}/${item.picture_file}`}
              title={item.title}
            />
          )}
        />
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#000',
    flex: 1,
  },
});
