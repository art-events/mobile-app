import React from 'react'
import PropTypes from 'prop-types'
// import { Ionicons } from '@expo/vector-icons';
import i18n from 'i18n-js';
import {
  ActivityIndicator,
  Animated,
  Button,
  Easing,
  KeyboardAvoidingView,
  LayoutAnimation,
  Platform,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  View
} from 'react-native'
import {
  TransitionPresets,
  createStackNavigator
} from '@react-navigation/stack';
import Layout from '@constants/Layout'
import Routes from '@constants/Routes'
import { AuthContext, AuthStateContext, UserStateContext } from '@hooks/auth'


const PasswordInput = ({ onChangeText, placeholder, style, value }) => (
  <FormInput
    autoCompleteType={'password'}
    maxLength={32}
    onChangeText={onChangeText}
    placeholder={placeholder}
    placeholderTextColor={'#777'}
    secureTextEntry={true}
    style={style}
    type={'password'}
    value={value}
  />
)

PasswordInput.propTypes = {
  onChangeText: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  style: PropTypes.object,
  value: PropTypes.string.isRequired,
}

PasswordInput.defaultProps = {
  style: {},
}

const EmailTextInput = ({
  autoFocus,
  onChangeText,
  placeholder,
  style,
  value,
}) => (
  <FormInput
    autoCompleteType={'email'}
    autoFocus={autoFocus}
    keyboardType={'email-address'}
    onChangeText={onChangeText}
    placeholder={placeholder}
    placeholderTextColor={'#777'}
    style={style}
    value={value}
  />
)

EmailTextInput.propTypes = {
  autoFocus: PropTypes.bool,
  onChangeText: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  style: PropTypes.object,
  value: PropTypes.string.isRequired,
}

EmailTextInput.defaultProps = {
  autoFocus: false,
  style: {},
}

const UsernameTextInput = ({ onChangeText, placeholder, value }) => (
  <FormInput
    autoCompleteType={'username'}
    maxLength={32}
    onChangeText={onChangeText}
    placeholder={placeholder}
    placeholderTextColor={'#777'}
    value={value}
  />
)

UsernameTextInput.propTypes = {
  onChangeText: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string.isRequired,
}

const FormInput = ({ style, ...props }) => (
  <TextInput
    {...props}
    style={[styles.textInput, styles.formElement, { color: '#fafafa' }, style]}
  />
)

FormInput.propTypes = {
  style: PropTypes.object,
}

const GoBackButton = (props) => (
  <Button
    color={'#000'}
    title={i18n.t('auth-form-go-back-button')}
    {...props}
  />
)

const AuthStack = createStackNavigator();

export default function AuthenticationStackScreen() {
  
  return (
    <AuthStack.Navigator
      headerMode={'none'}
      initialRouteName={Routes.AuthenticationScreen}
      mode={'modal'}
      screenOptions={() => ({
        ...TransitionPresets.SlideFromRightIOS,
      })}
    >
      <AuthStack.Screen
        component={AuthenticationScreen}
        name={Routes.AuthenticationScreen}
      />
      <AuthStack.Screen
        component={SignInScreen}
        name={Routes.SignInScreen}
      />
      <AuthStack.Screen
        component={SignUpScreen}
        name={Routes.SignUpScreen}
      />
    </AuthStack.Navigator>
  )
}

AuthenticationScreen.propTypes = {
  navigation: PropTypes.object.isRequired
}

function AuthenticationScreen({ navigation }) {
  return (
    <SafeAreaView style={{flex:1}}>
      <KeyboardAvoidingView
        behavior={'padding'}
        keyboardVerticalOffset={-30}
        style={{ flex: 1 }}
      >
        <View style={styles.container}>
          <View style={styles.box}>
            <View style={styles.hintMessageContainer}>
              <Text style={styles.hintMessageText}>
                {i18n.t('auth-screen-message')}
              </Text>
            </View>
            <View style={styles.actionButtons}>
              <View style={styles.formElement}>
                <Button
                  color={'#000'}
                  onPress={() => {
                    navigation.navigate(Routes.SignInScreen)
                  }}
                  title={i18n.t('auth-screen-sign-in-button')}
                />
              </View>
              <View style={styles.formElement}>
                <Button
                  color={'#000'}
                  onPress={() => {
                    navigation.navigate(Routes.SignUpScreen)
                  }}
                  title={i18n.t('auth-screen-sign-up-button')}
                />
              </View>
            </View>
          </View>
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  )
}

SignInScreen.propTypes = {
  navigation: PropTypes.object.isRequired
}

function SignInScreen({ navigation }) {
  const [emailState, setEmailState] = React.useState('')
  const [passwordState, setPasswordState] = React.useState('')
  const [isInvalidEmailState, setInvalidEmailState] = React.useState(false)

  const authContext = React.useContext(AuthContext)
  const { isLoading, hasErrors, userToken } = React.useContext(AuthStateContext)
  const user = React.useContext(UserStateContext)

  const animatedValue = new Animated.Value(0)

  const handleSignIn = async () => {
    if (isInvalidEmailState) {
      handleAnimation(animatedValue)
      return
    }
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring)
    await authContext.signIn({ username: emailState, password: passwordState })
  }

  function handleEmailChange(value) {
    setInvalidEmailState(false)
    setEmailState(value)
    const isValidEmail = (email) => {
      /* eslint-disable-next-line */
      const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{1,}))$/;
      return re.test(String(email).toLowerCase());
    }
    if (!isValidEmail(emailState)) {
      setInvalidEmailState(true)
    }
  }

  function handlePasswordChange(value) {
    setPasswordState(value)
  }

  React.useEffect(() => {
    if (hasErrors) {
      handleAnimation(animatedValue)
    }
    if (userToken !== null) {
      navigation.navigate(Routes.FeedScreen)
    }
    return () => {}
  }, [hasErrors, user, userToken])

  return (
    <SafeAreaView style={{flex:1}}>
      <KeyboardAvoidingView
        behavior={'padding'}
        keyboardVerticalOffset={-30}
        style={{ flex: 1 }}
      >
        <View style={styles.container}>
        {
          isLoading ? (
            <View style={styles.activityIndicator}>
              <ActivityIndicator
                animating={isLoading}
                color="#fafafa"
                size="large"
              />
            </View>
          ) : null
        }
          <Animated.View style={[styles.box, {
              transform: [{
                translateX: animatedValue.interpolate({
                  inputRange: [-100, 100],
                  outputRange: [-100, 100],
                })
              }]
            }]}
          >
            <View style={styles.hintMessageContainer}>
              <Text style={styles.hintMessageText}>
                {i18n.t('sign-in-screen-message')}
              </Text>
            </View>
            <View style={styles.actionButtons}>
              <EmailTextInput
                onChangeText={handleEmailChange}
                placeholder={i18n.t('auth-form-email-placeholder')}
                style={{
                  color: isInvalidEmailState ? '#ff3333' : '#fafafa'
                }}
                value={emailState}
              />
              <PasswordInput
                onChangeText={handlePasswordChange}
                placeholder={i18n.t('auth-form-password-placeholder')}
                value={passwordState}
              />
              <View style={styles.formElement}>
                <Button
                  onPress={handleSignIn}
                  title={i18n.t('sign-in-screen-submit-button')}
                />
              </View>
              <View style={styles.formElement}>
                <GoBackButton
                  onPress={() => {
                    navigation.navigate(Routes.AuthenticationScreen)
                  }}
                />
              </View>
            </View>
          </Animated.View>
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  )
}

SignUpScreen.propTypes = {
  navigation: PropTypes.object.isRequired
}

function SignUpScreen({ navigation }) {
  const [emailState, setEmailState] = React.useState('')
  const [passwordRepeatState, setPasswordRepeatState] = React.useState('')
  const [passwordState, setPasswordState] = React.useState('')
  const [usernameState, setUsernameState] = React.useState('')
  const animatedValue = new Animated.Value(0)

  // function signUp() {
  //   const isValidPassword = (password) => {
  //     const re = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$"
  //     return re.test(String(email).toLowerCase());
  //   }
  // }

  return (
    <SafeAreaView style={{flex:1}}>
      <KeyboardAvoidingView
        behavior={'padding'}
        keyboardVerticalOffset={-30}
        style={{ flex: 1 }}
      >
        <View style={styles.container}>
          <Animated.View style={[styles.box, {
              transform: [{
                translateX: animatedValue.interpolate({
                  inputRange: [-100, 100],
                  outputRange: [-100, 100],
                })
              }]
            }]}
          >
            <View style={styles.hintMessageContainer}>
              <Text style={styles.hintMessageText}>
                {i18n.t('sign-up-screen-message')}
              </Text>
            </View>
            <View style={styles.actionButtons}>
              <EmailTextInput
                onChangeText={setEmailState}
                placeholder={i18n.t('auth-form-email-placeholder')}
                value={emailState}
              />
              <UsernameTextInput
                onChangeText={setUsernameState}
                placeholder={i18n.t('auth-form-username-placeholder')}
                value={usernameState}
              />
              <PasswordInput
                onChangeText={setPasswordState}
                placeholder={i18n.t('auth-form-password-placeholder')}
                value={passwordState}
              />
              <PasswordInput
                onChangeText={setPasswordRepeatState}
                placeholder={i18n.t('auth-form-repeat-password-placeholder')}
                value={passwordRepeatState}
              />
              <View style={styles.formElement}>
                <Button
                  onPress={() => handleAnimation(animatedValue)}
                  title={i18n.t('sign-up-screen-submit-button')}
                />
              </View>
              <View style={styles.formElement}>
                <GoBackButton
                  onPress={() => {
                    navigation.navigate(Routes.AuthenticationScreen)
                  }}
                />
              </View>
            </View>
          </Animated.View>
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  )
}

/**
 * handleAnimation
 * Reusable function to transform for a fixex period of time
 */
const handleAnimation = (animatedValue, iterations = 5) => {
    // A loop is needed for continuous animation
    Animated.loop(
      // Animation consists of a sequence of steps
      Animated.sequence([
        // starts transformation in one direction (only half the time is needed)
        Animated.timing(
          animatedValue, {
            toValue: 3.0,
            duration: 30,
            easing: Easing.linear,
            useNativeDriver: true
          }),
        // transform in other direction, to minimum value
        // (equal or twice the duration of above)
        Animated.timing(animatedValue, {
          toValue: -3.0,
          duration: 60,
          easing: Easing.linear,
          useNativeDriver: true
        }),
        // return to begin position
        Animated.timing(animatedValue, {
          toValue: 0.0,
          duration: 30,
          easing: Easing.linear,
          useNativeDriver: true
        })
      ]),
    { iterations }).start()
  }

const styles = StyleSheet.create({
  actionButtons: {
    paddingLeft: 30,
    paddingRight: 30,
  },
  activityIndicator: {
    alignItems: 'center',
    backgroundColor: '#000000BB',
    flex: 1,
    height: Layout.window.height,
    justifyContent: 'center',
    position: 'absolute',
    width: Layout.window.width,
    zIndex: 10,
  },
  box: {
    backgroundColor: '#fafafa',
    borderColor: '#000',
    borderRadius: 20,
    borderWidth: 1,
    justifyContent: 'space-evenly',
    minHeight: Layout.window.height / 1.8,
    width: Layout.window.width / 1.2,
  },
  container: {
    alignItems: 'center',
    backgroundColor: '#000',
    flex: 1,
    height: Layout.window.height,
    justifyContent: Platform.OS === 'android' ? 'center' : 'flex-start',
    paddingTop: Platform.OS === 'android' ? 0 : 70,
  },
  formElement: {
    margin: 10,
  },
  hintMessageContainer: {
    alignSelf: 'center',
    margin: 15,
  },
  hintMessageText: {
    fontSize: 22,
  },
  textInput: {
    backgroundColor: '#000',
    borderRadius: 5,
    fontSize: 18,
    height: 34,
    paddingLeft: 5,
    paddingRight: 5,
    textAlign: 'center',
  }
})
